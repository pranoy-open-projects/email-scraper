
import pandas as pd

def remove_duplicates(path):
    df = pd.read_csv(path)
    df = df.drop_duplicates(subset='email')
    print(df)
    df.to_csv(path, mode='w',header=True)
    return df

if __name__ == '__main__':
    bad_words = ['facebook', 'instagram', 'youtube', 'twitter', 'wiki']
    path = 'data.csv'
    df = remove_duplicates(path)